$(document).ready(function () {
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/sw.js').then(function () {
            console.log('SW inscrit');
        });
    }
    $(document).click(function (event) {
        if ($(event.target).not(".hamburger") && $(event.target).not("header nav")) {
            if ($("header nav").hasClass("is-active")) {
                $("header nav").removeClass("is-active");
                return;
            }
            $("header nav").toggleClass("is-active");
        }
    });
});