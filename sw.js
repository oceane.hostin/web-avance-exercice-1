let cache_fichier = [
    '/index.html',
    '/css/style.css',
    '/css/reset.css',
    '/js/app.js',
    '/js/jquery.min.js'
];

self.addEventListener("install", function (event) {
    console.log("[SW] Install Event");
    self.skipWaiting();
    event.waitUntil(
        caches.open("cache-v1").then(function (cache) {
            console.log("Mise en cache des fichiers");
            return cache.addAll(cache_fichier);
        })
    )
});

self.addEventListener("activate", function () {
    console.log("[SW] Activate Event test");
});

self.addEventListener("fetch", function (event) {
    console.log("[SW] Fetch Event");
    console.log(event.request);
    /*if(event.request.url == "http://localhost:5000/page2") {
        event.respondWith(
            new Response("Pas de chance ...")
        )
    }*/
    event.respondWith(
        fromCache(event.request)
            .then(function (response) {
                console.warn("Il y a bien correspondance"+response);
                return response;
            })
            .catch(function (response) {
                console.warn("Le fichier n'est pas en cache");
                return fetch(event.request);
            })
    )
});

function fromCache(request) {
    return caches.open("cache-v1").then(function (cache) {
        return cache.match(request).then(function (matching) {
            if (!matching || matching.status === 404) {
                return Promise.reject("no-match");
            }
            else {
                return matching;
            }
        })
    })
}